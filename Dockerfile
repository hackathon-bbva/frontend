FROM nginx:1.18.0-alpine

ARG DIST_FOLDER=dist/frontend
ARG NGINX_CONFIG=.nginx/default.conf

COPY ${NGINX_CONFIG} /etc/nginx/conf.d/default.conf
COPY ${DIST_FOLDER} /usr/share/nginx/html

EXPOSE 80
