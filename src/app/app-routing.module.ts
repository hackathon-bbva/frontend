import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BancaMultipleComponent } from './banca-multiple/banca-multiple.component';
import { BanxicoComponent } from './banxico/banxico.component';
import { CNBVComponent } from './cnbv/cnbv.component';
import { DeCaptacionComponent } from './de-captacion/de-captacion.component';
import { EstatalComponent } from './estatal/estatal.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NacionalComponent } from './nacional/nacional.component';
import { OperativasComponent } from './operativas/operativas.component';
import { PorSectorComponent } from './por-sector/por-sector.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'home', component: HomePageComponent },

  /* CNBV PROVIDE ROUTES */
  { path: 'home/cnbv', component: CNBVComponent },

  { path: 'home/cnbv/nacional', component: NacionalComponent },

  { path: 'home/cnbv/nacional/banca-multiple', component: BancaMultipleComponent },

  { path: 'home/cnbv/nacional/por-sector', component: PorSectorComponent},

  { path: 'home/cnbv/nacional/por-sector/de-captacion', component: DeCaptacionComponent },

  { path: 'home/cnbv/nacional/por-sector/operativas', component: OperativasComponent },

  { path: 'home/cnbv/nacional/banca-multiple/de-captacion', component: DeCaptacionComponent },

  { path: 'home/cnbv/nacional/banca-multiple/operativas', component: OperativasComponent },


  { path: 'home/cnbv/estatal', component: EstatalComponent },  

  { path: 'home/cnbv/estatal/por-sector', component: PorSectorComponent },

  { path: 'home/cnbv/estatal/banca-multiple', component: BancaMultipleComponent},

  { path: 'home/cnbv/estatal/por-sector/de-captacion', component: DeCaptacionComponent },

  { path: 'home/cnbv/estatal/por-sector/operativas', component: OperativasComponent },  

  { path: 'home/cnbv/estatal/banca-multiple/de-captacion', component: DeCaptacionComponent },

  { path: 'home/cnbv/estatal/banca-multiple/operativas', component: OperativasComponent },

  
  
  
  { path: 'home/cnbv/estatal/por-sector/de-captacion', component: DeCaptacionComponent },

  /* Banxico PROVIDE ROUTES */
  { path: 'home/banxico', component: CNBVComponent },

  { path: 'home/banxico/nacional', component: NacionalComponent },

  { path: 'home/banxico/nacional/banca-multiple', component: BancaMultipleComponent },

  { path: 'home/banxico/nacional/por-sector', component: PorSectorComponent},

  { path: 'home/banxico/nacional/por-sector/de-captacion', component: DeCaptacionComponent },

  { path: 'home/banxico/nacional/por-sector/operativas', component: OperativasComponent },

  { path: 'home/banxico/nacional/banca-multiple/de-captacion', component: DeCaptacionComponent },

  { path: 'home/banxico/nacional/banca-multiple/operativas', component: OperativasComponent },


  { path: 'home/banxico/estatal', component: EstatalComponent },  

  { path: 'home/banxico/estatal/por-sector', component: PorSectorComponent },

  { path: 'home/banxico/estatal/banca-multiple', component: BancaMultipleComponent},

  { path: 'home/banxico/estatal/por-sector/de-captacion', component: DeCaptacionComponent },

  { path: 'home/banxico/estatal/por-sector/operativas', component: OperativasComponent },  

  { path: 'home/banxico/estatal/banca-multiple/de-captacion', component: DeCaptacionComponent },

  { path: 'home/banxico/estatal/banca-multiple/operativas', component: OperativasComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
