import { Component, OnInit } from '@angular/core';
import { ScrappingApiService } from './scrapping-api.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';

  public data: string;

  constructor(private apiService: ScrappingApiService) {
  }

  async ngOnInit() {
    this.data = await this.apiService.getAllData();
    console.log(this.data);
  }
}
