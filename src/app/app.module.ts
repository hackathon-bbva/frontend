import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SafePipe } from './widget/widget.component';
import { WidgetComponent } from './widget/widget.component';
import { CNBVComponent } from './cnbv/cnbv.component';
import { NacionalComponent } from './nacional/nacional.component';
import { EstatalComponent } from './estatal/estatal.component';
import { BancaMultipleComponent } from './banca-multiple/banca-multiple.component';
import { PorSectorComponent } from './por-sector/por-sector.component';
import { DeCaptacionComponent } from './de-captacion/de-captacion.component';
import { OperativasComponent } from './operativas/operativas.component';
import { HomePageComponent } from './home-page/home-page.component';
import { BanxicoComponent } from './banxico/banxico.component';


@NgModule({
  declarations: [
    AppComponent,
    WidgetComponent,
    SafePipe,
    CNBVComponent,
    NacionalComponent,
    EstatalComponent,
    BancaMultipleComponent,
    PorSectorComponent,
    DeCaptacionComponent,
    OperativasComponent,
    HomePageComponent,
    BanxicoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
