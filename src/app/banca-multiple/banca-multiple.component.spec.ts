import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BancaMultipleComponent } from './banca-multiple.component';

describe('BancaMultipleComponent', () => {
  let component: BancaMultipleComponent;
  let fixture: ComponentFixture<BancaMultipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BancaMultipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BancaMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
