import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banca-multiple',
  templateUrl: './banca-multiple.component.html',
  styleUrls: ['./banca-multiple.component.css']
})
export class BancaMultipleComponent implements OnInit {
/*guarda la ruta completa actual */
miRutaActual: string = window.location.href;
mensaje: string;
mensaje2:string;
mensaje3:string;
rutaDeCaptacion: string;
rutaDeOperativas: string;

/*Verificamos cual es la ruta actual para cambiar el mensaje1 y mensaje2 que se
muestra en el html del componente */
ngOnInit(): void {
  let ruta = this.miRutaActual.indexOf("cnbv");
  if (ruta != -1) {
    this.mensaje = "Comisión Nacional Bancaria y de Valores";  
  } else {
    this.mensaje = "Banxico";
  }

  ruta = this.miRutaActual.indexOf("estatal");
  if(ruta != -1){
    if(this.mensaje == "Comisión Nacional Bancaria y de Valores"){
      this.mensaje2 = "Estatal"; 
      this.rutaDeCaptacion = "/home/cnbv/estatal/banca-multiple/de-captacion";
      this.rutaDeOperativas = "/home/cnbv/estatal/banca-multiple/operativas";
    }else{
      this.mensaje2 = "Estatal"; 
      this.rutaDeCaptacion = "/home/banxico/estatal/banca-multiple/de-captacion";
      this.rutaDeOperativas = "/home/banxico/estatal/banca-multiple/operativas";
    }
  }else{
    if(this.mensaje == "Comisión Nacional Bancaria y de Valores"){
      this.mensaje2 = "Nacional"
      this.rutaDeCaptacion = "/home/cnbv/nacional/banca-multiple/de-captacion";
      this.rutaDeOperativas = "/home/cnbv/nacional/banca-multiple/operativas";
    }else{
      this.mensaje2 = "Nacional"
      this.rutaDeCaptacion = "/home/banxico/nacional/banca-multiple/de-captacion";
      this.rutaDeOperativas = "/home/banxico/nacional/banca-multiple/operativas";
    }
  }
}
}
