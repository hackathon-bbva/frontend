import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CNBVComponent } from './cnbv.component';

describe('CNBVComponent', () => {
  let component: CNBVComponent;
  let fixture: ComponentFixture<CNBVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CNBVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CNBVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
