import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeCaptacionComponent } from './de-captacion.component';

describe('DeCaptacionComponent', () => {
  let component: DeCaptacionComponent;
  let fixture: ComponentFixture<DeCaptacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeCaptacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeCaptacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
