import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-de-captacion',
  templateUrl: './de-captacion.component.html',
  styleUrls: ['./de-captacion.component.css']
})
export class DeCaptacionComponent implements OnInit {
  urlDeWidget: string;

  ngOnInit(): void {    
  }

  /*Funcion que asigna un valor urlDeWidget dependiendo de la ruta desde
  donde es llamado el componente.
  Retorla la liga para el iframe*/
  asignaUrlDeWidget():string{
    let rutaActual: string = window.location.href;
    console.log(rutaActual);
    switch (rutaActual) {
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/cnbv/nacional/por-sector/de-captacion":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_cap/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/cnbv/nacional/banca-multiple/de-captacion":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_cap/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/cnbv/estatal/por-sector/de-captacion":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_cap/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/cnbv/estatal/banca-multiple/de-captacion":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_cap/?anio=2020?mes=05";
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/banxico/nacional/por-sector/de-captacion":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_cap/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/banxico/nacional/banca-multiple/de-captacion":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_cap/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/banxico/estatal/por-sector/de-captacion":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_cap/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/banxico/estatal/banca-multiple/de-captacion":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_cap/?anio=2020?mes=05";//
        break;
      default:
        break;
    }
    return this.urlDeWidget;
  }
}
