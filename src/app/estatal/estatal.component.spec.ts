import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstatalComponent } from './estatal.component';

describe('EstatalComponent', () => {
  let component: EstatalComponent;
  let fixture: ComponentFixture<EstatalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstatalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstatalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
