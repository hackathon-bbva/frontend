import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-estatal',
  templateUrl: './estatal.component.html',
  styleUrls: ['./estatal.component.css']
})
export class EstatalComponent implements OnInit {

  /*guarda la ruta completa actual */
  miRutaActual:string = window.location.href ;
  mensaje:string;
  rutaDeBancaMultiple:string;
  rutaDePorSector:string;  

  /*Verificamos cual es la ruta actual para cambiar el mensaje que se
  muestra en el html del componente */
  ngOnInit(): void {
    let ruta = this.miRutaActual.indexOf("/home/cnbv/estatal");    
    if(ruta != -1){
      this.mensaje = "Comisión Nacional Bancaria y de Valores";
      this.rutaDeBancaMultiple = "/home/cnbv/estatal/banca-multiple";
      this.rutaDePorSector = "/home/cnbv/estatal/por-sector";
      
    }else{
      this.mensaje = "Banxico";
      this.rutaDeBancaMultiple = "/home/banxico/estatal/banca-multiple";
      this.rutaDePorSector = "/home/banxico/estatal/por-sector";
    }        
  }
}