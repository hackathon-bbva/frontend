import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nacional',
  templateUrl: './nacional.component.html',
  styleUrls: ['./nacional.component.css']
})
export class NacionalComponent implements OnInit {
  /*guarda la ruta completa actual */
  miRutaActual:string = window.location.href ;
  mensaje:string;
  rutaDeBancaMultiple:string;
  rutaDePorSector:string;   
 
  /*Verificamos cual es la ruta actual para cambiar el mensaje que se
  muestra en el html del componente */
  ngOnInit(): void {
    let ruta = this.miRutaActual.indexOf("/home/cnbv/nacional");    
    if(ruta != -1){
      this.mensaje = "Comisión Nacional Bancaria y de Valores";
      this.rutaDeBancaMultiple = "/home/cnbv/nacional/banca-multiple";
      this.rutaDePorSector = "/home/cnbv/nacional/por-sector";
    }else{
      this.mensaje = "Banxico";
      this.rutaDeBancaMultiple = "/home/banxico/nacional/banca-multiple";
      this.rutaDePorSector = "/home/banxico/nacional/por-sector";
    }        
  }

}
