import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperativasComponent } from './operativas.component';

describe('OperativasComponent', () => {
  let component: OperativasComponent;
  let fixture: ComponentFixture<OperativasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperativasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperativasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
