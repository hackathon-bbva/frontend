import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-operativas',
  templateUrl: './operativas.component.html',
  styleUrls: ['./operativas.component.css']
})
export class OperativasComponent implements OnInit {
  urlDeWidget: string;

  ngOnInit(): void {
  }

  asignaUrlDeWidget(): string {
    let rutaActual: string = window.location.href;
    console.log(rutaActual);
    switch (rutaActual) {
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/cnbv/nacional/por-sector/operativas":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_ope/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/cnbv/nacional/banca-multiple/operativas":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_ope/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/cnbv/estatal/por-sector/operativas":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_ope/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/cnbv/estatal/banca-multiple/operativas":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_ope/?anio=2020?mes=05";
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/banxico/nacional/por-sector/operativas":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_ope/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/banxico/nacional/banca-multiple/operativas":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_ope/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/banxico/estatal/por-sector/operativas":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_ope/?anio=2020?mes=05";//
        break;
      
      case "http://datastream-1293247667.us-east-2.elb.amazonaws.com/home/banxico/estatal/banca-multiple/operativas":
        this.urlDeWidget = "http://datastream-1293247667.us-east-2.elb.amazonaws.com:8050/apps/app_geo_ope/?anio=2020?mes=05";//
        break;
      default:
        break;
    }
    return this.urlDeWidget;
  }

}
