import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-por-sector',
  templateUrl: './por-sector.component.html',
  styleUrls: ['./por-sector.component.css']
})
export class PorSectorComponent implements OnInit {
  /*guarda la ruta completa actual */
  miRutaActual: string = window.location.href;
  mensaje: string;
  mensaje2:string;
  mensaje3:string;
  rutaDeCaptacion: string;
  rutaDeOperativas: string;

  /*Verificamos cual es la ruta actual para cambiar el mensaje1 y mensaje2 que se
  muestra en el html del componente */
  ngOnInit(): void {
    let ruta = this.miRutaActual.indexOf("cnbv");
    if (ruta != -1) {
      this.mensaje = "Comisión Nacional Bancaria y de Valores";  
    } else {
      this.mensaje = "Banxico";
    }

    ruta = this.miRutaActual.indexOf("estatal");
    if(ruta != -1){
      if(this.mensaje == "Comisión Nacional Bancaria y de Valores"){
        this.mensaje2 = "Estatal"; 
        this.rutaDeCaptacion = "/home/cnbv/estatal/por-sector/de-captacion";
        this.rutaDeOperativas = "/home/cnbv/estatal/por-sector/operativas";
      }else{
        this.mensaje2 = "Estatal"; 
        this.rutaDeCaptacion = "/home/banxico/estatal/por-sector/de-captacion";
        this.rutaDeOperativas = "/home/banxico/estatal/por-sector/operativas";
      }
    }else{
      if(this.mensaje == "Comisión Nacional Bancaria y de Valores"){
        this.mensaje2 = "Nacional"
        this.rutaDeCaptacion = "/home/cnbv/nacional/por-sector/de-captacion";
        this.rutaDeOperativas = "/home/cnbv/nacional/por-sector/operativas";
      }else{
        this.mensaje2 = "Nacional"
        this.rutaDeCaptacion = "/home/banxico/nacional/por-sector/de-captacion";
        this.rutaDeOperativas = "/home/banxico/nacional/por-sector/operativas";
      }
    }
  }
}
