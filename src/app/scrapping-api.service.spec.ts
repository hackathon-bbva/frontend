import { TestBed } from '@angular/core/testing';

import { ScrappingApiService } from './scrapping-api.service';

describe('ScrappingApiService', () => {
  let service: ScrappingApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScrappingApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
