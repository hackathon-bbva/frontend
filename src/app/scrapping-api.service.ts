import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScrappingApiService {

  private baseUrl: string;

  constructor(private httpClient: HttpClient) {
    this.baseUrl = "http://18.188.223.173:8052/cnvb/068";
  }

  getAllData(): Promise<any> {
    return this.httpClient.get<any>(this.baseUrl).toPromise();
  }
}
