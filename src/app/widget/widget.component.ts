import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-widget',
  template: `<iframe [width]=1900 [height]=710 [src]="url | safe" style="border:none;"></iframe>`,
})

export class WidgetComponent {
  @Input() url: string;
  @Input() width: string;
  @Input() height: string;
}

import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}